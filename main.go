/**
 *
 * @author: R. Heriniaina
 *
 */
package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Structure des données qu'on en a besoin
type Country struct {
	Name struct {
		Official string `json:"official"`
	} `json:"name"`
	Cca2       string `json:"cca2"`
	Cca3       string `json:"cca3"`
	Region     string `json:"region"`
	Subregion  string `json:"subregion"`
	Currencies map[string]struct {
		Name   string `json:"name"`
		Symbol string `json:"symbol"`
	} `json:"currencies"`
}

// Structure demandé par le client
type Countries struct {
	Name           string
	Cca2           string
	Cca3           string
	Region         string
	Subregion      string
	Currency       string
	CurrencySymbol string
}

func main() {

	response, err := http.Get("https://restcountries.com/v3.1/all")
	if err != nil {
		fmt.Println("Erreur lors de la requête GET:", err)
		return
	}
	defer response.Body.Close()

	if response.StatusCode == http.StatusOK {
		var countries []Country
		decodeErr := json.NewDecoder(response.Body).Decode(&countries)
		if decodeErr != nil {
			fmt.Println("Erreur lors de l'analyse JSON:", decodeErr)
			return
		}

		var countriesData []Countries

		// Filtre pour les pays d'Europe et North America
		// J'aurai préférer faire un filtre par continent, mais je vais me servir seulement des champs demandé
		// Du coup pour les pays d'Europe j'ai fait un filtre par Region et pour les North America, il n'y a pas de Region North America alors j'ai filtrer par Subregion
		for _, country := range countries {
			if country.Region == "Europe" || country.Region == "North America" {
				var currency, currencySymbol string
				for _, currencyData := range country.Currencies {
					currency = currencyData.Name
					currencySymbol = currencyData.Symbol
					break // On ne prend que la première paire clé-valeur du champ "Currencies"
				}

				countryData := Countries{
					Name:           country.Name.Official,
					Cca2:           country.Cca2,
					Cca3:           country.Cca3,
					Region:         country.Region,
					Subregion:      country.Subregion,
					Currency:       currency,
					CurrencySymbol: currencySymbol,
				}
				countriesData = append(countriesData, countryData)
			}
		}

		// Affichage des données avec la structure demandé
		for _, country := range countriesData {
			fmt.Printf("Nom : %s, Cca2 : %s, Cca3 : %s, Région : %s, Sous-région : %s, Devise : %s, Symbole de la devise : %s\n", country.Name, country.Cca2, country.Cca3, country.Region, country.Subregion, country.Currency, country.CurrencySymbol)
		}
	} else {
		fmt.Printf("Code de statut HTTP non géré : %d\n", response.StatusCode)
	}
}
